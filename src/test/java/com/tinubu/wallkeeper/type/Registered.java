/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.type;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Objects;
import java.util.StringJoiner;

/** Registered type in scope factories. */
public class Registered implements SuperType {

   protected final String value;

   public Registered(String value) {
      this.value = notNull(value);
   }

   public Registered() {
      this("any");
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Registered that = (Registered) o;
      return Objects.equals(value, that.value);
   }

   @Override
   public int hashCode() {
      return Objects.hash(value);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", Registered.class.getSimpleName() + "[", "]")
            .add("value='" + value + "'")
            .toString();
   }

}
