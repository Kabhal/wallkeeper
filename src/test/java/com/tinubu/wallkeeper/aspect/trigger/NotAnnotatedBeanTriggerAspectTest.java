/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.trigger;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.tinubu.wallkeeper.NotRegisteredScopeException;
import com.tinubu.wallkeeper.ParameterScopeException;
import com.tinubu.wallkeeper.ReturnObjectScopeException;
import com.tinubu.wallkeeper.aspect.AbstractScopeAspectTest;
import com.tinubu.wallkeeper.type.Registered;

@ContextConfiguration(classes = NotAnnotatedBeanTriggerAspectTest.class)
public class NotAnnotatedBeanTriggerAspectTest extends AbstractScopeAspectTest {

   @Autowired
   NotAnnotatedBean bean;

   @Test
   public void testVoidReturningNotAnnotatedMethod() {
      setScopeResult(true);

      bean.voidReturningNotAnnotatedMethod();

      setScopeResult(false);

      bean.voidReturningNotAnnotatedMethod();
   }

   @Test
   public void testUnRegisteredReturningNotAnnotatedMethod() {
      setScopeResult(true);

      bean.unRegisteredReturningNotAnnotatedMethod();

      setScopeResult(false);

      bean.unRegisteredReturningNotAnnotatedMethod();
   }

   @Test
   public void testUnRegisteredReturningNotAnnotatedMethodWithAnnotatedParameter() {
      setScopeResult(true);

      bean.unRegisteredReturningNotAnnotatedMethodWithAnnotatedParameter(new Registered());

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.unRegisteredReturningNotAnnotatedMethodWithAnnotatedParameter(new Registered()))
            .withMessage(
                  "Scope check failed for 'parameter=Registered[value='any']' parameter of 'com.tinubu.wallkeeper.aspect.trigger.NotAnnotatedBean::unRegisteredReturningNotAnnotatedMethodWithAnnotatedParameter' operation using 'null' scope context");
   }

   @Test
   public void testNotNotAnnotatedMethod() {
      setScopeResult(true);

      bean.notAnnotatedMethod();

      setScopeResult(false);

      bean.notAnnotatedMethod();
   }

   @Test
   public void testNotNotAnnotatedMethodWithNotAnnotatedParameter() {
      setScopeResult(true);

      bean.notAnnotatedMethodWithNotAnnotatedParameter(new Registered());

      setScopeResult(false);

      bean.notAnnotatedMethodWithNotAnnotatedParameter(new Registered());
   }

   @Test
   public void testNotNotAnnotatedMethodWithAnnotatedParameter() {
      setScopeResult(true);

      bean.notAnnotatedMethodWithAnnotatedParameter(new Registered());

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.notAnnotatedMethodWithAnnotatedParameter(new Registered()))
            .withMessage(
                  "Scope check failed for 'parameter=Registered[value='any']' parameter of 'com.tinubu.wallkeeper.aspect.trigger.NotAnnotatedBean::notAnnotatedMethodWithAnnotatedParameter' operation using 'null' scope context");
   }

   @Test
   public void testVoidReturningAnnotatedMethod() {
      setScopeResult(true);

      bean.voidReturningAnnotatedMethod();

      setScopeResult(false);

      bean.voidReturningAnnotatedMethod();
   }

   @Test
   public void testUnRegisteredReturningAnnotatedMethod() {
      setScopeResult(true);

      assertThatExceptionOfType(NotRegisteredScopeException.class)
            .isThrownBy(() -> bean.unRegisteredReturningAnnotatedMethod())
            .withMessage("Scope not registered for 'com.tinubu.wallkeeper.type.UnRegistered'");

      setScopeResult(false);

      assertThatExceptionOfType(NotRegisteredScopeException.class)
            .isThrownBy(() -> bean.unRegisteredReturningAnnotatedMethod())
            .withMessage("Scope not registered for 'com.tinubu.wallkeeper.type.UnRegistered'");
   }

   @Test
   public void testAnnotatedMethod() {
      setScopeResult(true);

      bean.annotatedMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.annotatedMethod())
            .withMessage(
                  "Scope check failed for 'Registered[value='any']' return object of 'com.tinubu.wallkeeper.aspect.trigger.NotAnnotatedBean::annotatedMethod' operation using 'null' scope context");
   }

   @Test
   public void testAnnotatedMethodWithAnnotatedParameter() {
      setScopeResult(true);

      bean.annotatedMethodWithAnnotatedParameter(new Registered());

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.annotatedMethodWithAnnotatedParameter(new Registered()))
            .withMessage(
                  "Scope check failed for 'parameter=Registered[value='any']' parameter of 'com.tinubu.wallkeeper.aspect.trigger.NotAnnotatedBean::annotatedMethodWithAnnotatedParameter' operation using 'null' scope context");
   }

   @Test
   public void testAnnotatedMethodWithNotAnnotatedParameter() {
      setScopeResult(true);

      bean.annotatedMethodWithNotAnnotatedParameter(new Registered());

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.annotatedMethodWithNotAnnotatedParameter(new Registered()))
            .withMessage(
                  "Scope check failed for 'Registered[value='any']' return object of 'com.tinubu.wallkeeper.aspect.trigger.NotAnnotatedBean::annotatedMethodWithNotAnnotatedParameter' operation using 'null' scope context");
   }

}