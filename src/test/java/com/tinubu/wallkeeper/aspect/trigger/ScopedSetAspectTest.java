/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.trigger;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.tinubu.wallkeeper.ParameterScopeException;
import com.tinubu.wallkeeper.ReturnObjectScopeException;
import com.tinubu.wallkeeper.aspect.AbstractScopeAspectTest;
import com.tinubu.wallkeeper.type.FailSensitive;
import com.tinubu.wallkeeper.type.Registered;
import com.tinubu.wallkeeper.type.Sensitive;

@ContextConfiguration(classes = ScopedSetAspectTest.class)
public class ScopedSetAspectTest extends AbstractScopeAspectTest {

   @Autowired
   ScopedSetAnnotatedBean bean;

   @Test
   public void testSingleScopedMethod() {
      setScopeResult(true);

      bean.singledScopedMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.singledScopedMethod())
            .withMessage(
                  "Scope check failed for 'Registered[value='any']' return object of 'com.tinubu.wallkeeper.aspect.trigger.ScopedSetAnnotatedBean::singledScopedMethod' operation using 'null' scope context");
   }

   @Test
   public void testSingleScopedMethodWithParameter() {
      setScopeResult(true);

      bean.singledScopedMethodWithParameter(new Registered());

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.singledScopedMethodWithParameter(new Registered()))
            .withMessage(
                  "Scope check failed for 'parameter=Registered[value='any']' parameter of 'com.tinubu.wallkeeper.aspect.trigger.ScopedSetAnnotatedBean::singledScopedMethodWithParameter' operation using 'null' scope context");
   }

   @Test
   public void testMultipleScopedMethod() {
      setScopeResult(true);

      bean.multipleScopedMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.multipleScopedMethod())
            .withMessage(
                  "Scope check failed for 'Registered[value='any']' return object of 'com.tinubu.wallkeeper.aspect.trigger.ScopedSetAnnotatedBean::multipleScopedMethod' operation using 'null' scope context");
   }

   @Test
   public void testMultipleScopedMethodWithParameter() {
      setScopeResult(true);

      bean.multipleScopedMethodWithParameter(new Registered());

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.multipleScopedMethodWithParameter(new Registered()))
            .withMessage(
                  "Scope check failed for 'parameter=Registered[value='any']' parameter of 'com.tinubu.wallkeeper.aspect.trigger.ScopedSetAnnotatedBean::multipleScopedMethodWithParameter' operation using 'null' scope context");
   }

   @Test
   public void testScopedSetMethod() {
      setScopeResult(true);

      bean.scopedSetMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.scopedSetMethod())
            .withMessage(
                  "Scope check failed for 'Registered[value='any']' return object of 'com.tinubu.wallkeeper.aspect.trigger.ScopedSetAnnotatedBean::scopedSetMethod' operation using 'null' scope context");
   }

   @Test
   public void testScopedSetMethodWithParameter() {
      setScopeResult(true);

      bean.scopedSetMethodWithParameter(new Registered());

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedSetMethodWithParameter(new Registered()))
            .withMessage(
                  "Scope check failed for 'parameter=Registered[value='any']' parameter of 'com.tinubu.wallkeeper.aspect.trigger.ScopedSetAnnotatedBean::scopedSetMethodWithParameter' operation using 'null' scope context");
   }

   @Test
   public void testMultipleIgnoredScopedMethod() {
      setScopeResult(true);

      bean.multipleIgnoredScopedMethod();

      setScopeResult(false);

      bean.multipleIgnoredScopedMethod();
   }

   @Test
   public void testMultipleIgnoredScopedMethodWithParameter() {
      setScopeResult(true);

      bean.multipleIgnoredScopedMethodWithParameter(new Registered());

      setScopeResult(false);

      bean.multipleIgnoredScopedMethodWithParameter(new Registered());
   }

   @Test
   public void testPartialFailureScopedMethod() {
      setScopeResult(true);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.partialFailureScopedMethod())
            .withMessage(
                  "Scope check failed for 'Registered[value='any']' return object of 'com.tinubu.wallkeeper.aspect.trigger.ScopedSetAnnotatedBean::partialFailureScopedMethod' operation using 'TestScopeContext[type=FAIL, data=null]' scope context");

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.partialFailureScopedMethod())
            .withMessage(
                  "Scope check failed for 'Registered[value='any']' return object of 'com.tinubu.wallkeeper.aspect.trigger.ScopedSetAnnotatedBean::partialFailureScopedMethod' operation using 'null' scope context");
   }

   @Test
   public void testPartialFailureScopedMethodWithParameter() {
      setScopeResult(true);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.partialFailureScopedMethodWithParameter(new Registered()))
            .withMessage(
                  "Scope check failed for 'parameter=Registered[value='any']' parameter of 'com.tinubu.wallkeeper.aspect.trigger.ScopedSetAnnotatedBean::partialFailureScopedMethodWithParameter' operation using 'TestScopeContext[type=FAIL, data=null]' scope context");

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.partialFailureScopedMethodWithParameter(new Registered()))
            .withMessage(
                  "Scope check failed for 'parameter=Registered[value='any']' parameter of 'com.tinubu.wallkeeper.aspect.trigger.ScopedSetAnnotatedBean::partialFailureScopedMethodWithParameter' operation using 'null' scope context");
   }

   @Test
   public void testInheritingScopedMethod() {
      setScopeResult(true);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.inheritingScopedMethod())
            .withMessage(
                  "Scope check failed for 'Sensitive[]' return object of 'com.tinubu.wallkeeper.aspect.trigger.ScopedSetAnnotatedBean::inheritingScopedMethod' operation using 'TestScopeContext[type=FAIL, data=null]' scope context");

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.inheritingScopedMethod())
            .withMessage(
                  "Scope check failed for 'Sensitive[]' return object of 'com.tinubu.wallkeeper.aspect.trigger.ScopedSetAnnotatedBean::inheritingScopedMethod' operation using 'null' scope context");
   }

   @Test
   public void testInheritingScopedMethodWithParameter() {
      setScopeResult(true);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.inheritingScopedMethodWithParameter(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.ScopedSetAnnotatedBean::inheritingScopedMethodWithParameter' operation using 'TestScopeContext[type=FAIL, data=null]' scope context");

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.inheritingScopedMethodWithParameter(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.ScopedSetAnnotatedBean::inheritingScopedMethodWithParameter' operation using 'null' scope context");
   }

   @Test
   public void testInheritingFailScopedMethod() {
      setScopeResult(true);

      bean.inheritingFailScopedMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.inheritingFailScopedMethod())
            .withMessage(
                  "Scope check failed for 'FailSensitive[]' return object of 'com.tinubu.wallkeeper.aspect.trigger.ScopedSetAnnotatedBean::inheritingFailScopedMethod' operation using 'null' scope context");
   }

   @Test
   public void testInheritingFailScopedMethodWithParameter() {
      setScopeResult(true);

      bean.inheritingFailScopedMethodWithParameter(new FailSensitive());

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.inheritingFailScopedMethodWithParameter(new FailSensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=FailSensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.ScopedSetAnnotatedBean::inheritingFailScopedMethodWithParameter' operation using 'null' scope context");
   }

}