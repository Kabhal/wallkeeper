/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.trigger;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.tinubu.wallkeeper.NotRegisteredScopeException;
import com.tinubu.wallkeeper.aspect.AbstractScopeAspectTest;
import com.tinubu.wallkeeper.type.UnRegistered;

@ContextConfiguration(classes = NotRegisteredTypeTriggerAspectTest.class)
public class NotRegisteredTypeTriggerAspectTest extends AbstractScopeAspectTest {

   @Autowired
   NotRegisteredTypeBean bean;

   @Test
   public void testNotAnnotatedMethod() {
      setScopeResult(true);

      bean.notAnnotatedMethod();
   }

   @Test
   public void testNotAnnotatedMethodWithNotAnnotatedParameter() {
      setScopeResult(true);

      bean.notAnnotatedMethodWithNotAnnotatedParameter(new UnRegistered());
   }

   @Test
   public void testNotAnnotatedMethodWithAnnotatedParameter() {
      setScopeResult(true);

      assertThatExceptionOfType(NotRegisteredScopeException.class)
            .isThrownBy(() -> bean.notAnnotatedMethodWithAnnotatedParameter(new UnRegistered()))
            .withMessage("Scope not registered for 'com.tinubu.wallkeeper.type.UnRegistered'");
   }

   @Test
   public void testAnnotatedMethod() {
      setScopeResult(true);

      assertThatExceptionOfType(NotRegisteredScopeException.class)
            .isThrownBy(() -> bean.annotatedMethod())
            .withMessage("Scope not registered for 'com.tinubu.wallkeeper.type.UnRegistered'");
   }

   @Test
   public void testAnnotatedMethodWithAnnotatedParameter() {
      setScopeResult(true);

      assertThatExceptionOfType(NotRegisteredScopeException.class)
            .isThrownBy(() -> bean.annotatedMethodWithAnnotatedParameter(new UnRegistered()))
            .withMessage("Scope not registered for 'com.tinubu.wallkeeper.type.UnRegistered'");
   }

   @Test
   public void testAnnotatedMethodWithNotAnnotatedParameter() {
      setScopeResult(true);

      assertThatExceptionOfType(NotRegisteredScopeException.class)
            .isThrownBy(() -> bean.annotatedMethodWithNotAnnotatedParameter(new UnRegistered()))
            .withMessage("Scope not registered for 'com.tinubu.wallkeeper.type.UnRegistered'");
   }

   @Test
   public void testNullReturnMethod() {
      setScopeResult(true);

      bean.nullReturnMethod();
   }

   @Test
   public void testNullParameterMethod() {
      setScopeResult(true);

      bean.nullParameterMethod(null);
   }

}