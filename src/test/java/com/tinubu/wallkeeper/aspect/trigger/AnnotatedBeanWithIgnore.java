/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.trigger;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.ScopedIgnored;
import com.tinubu.wallkeeper.type.Registered;

@Component
@ScopedBean
public class AnnotatedBeanWithIgnore {

   @Scoped(ignore = @ScopedIgnored)
   public Registered ignoredMethod() {
      return new Registered();
   }

   @Scoped(ignore = @ScopedIgnored)
   public Registered ignoredMethodWithNotAnnotatedParameter(Registered parameter) {
      return new Registered();
   }

   @Scoped(ignore = @ScopedIgnored)
   public Registered ignoredMethodWithAnnotatedParameter(@Scoped Registered parameter) {
      return new Registered();
   }

   @Scoped(ignore = @ScopedIgnored)
   public Registered ignoredMethodWithIgnoredParameter(
         @Scoped(ignore = @ScopedIgnored) Registered parameter) {
      return new Registered();
   }

   public void notIgnoredMethodWithIgnoredParameter(@Scoped(ignore = @ScopedIgnored) Registered parameter) {
   }

   @Scoped(ignore = @ScopedIgnored)
   public Optional<Registered> ignoredOptionalMethod() {
      return optional(new Registered());
   }

   public void notIgnoredMethodWithIgnoredOptionalParameter(
         @Scoped(ignore = @ScopedIgnored) Optional<Registered> parameter) {
   }

}
