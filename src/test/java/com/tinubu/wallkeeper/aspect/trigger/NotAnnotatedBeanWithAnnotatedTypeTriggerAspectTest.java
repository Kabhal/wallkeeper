/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.trigger;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.tinubu.wallkeeper.ParameterScopeException;
import com.tinubu.wallkeeper.ReturnObjectScopeException;
import com.tinubu.wallkeeper.aspect.AbstractScopeAspectTest;
import com.tinubu.wallkeeper.type.NonSensitive;
import com.tinubu.wallkeeper.type.Sensitive;

@ContextConfiguration(classes = NotAnnotatedBeanWithAnnotatedTypeTriggerAspectTest.class)
public class NotAnnotatedBeanWithAnnotatedTypeTriggerAspectTest extends AbstractScopeAspectTest {

   @Autowired
   NotAnnotatedBeanWithAnnotatedType bean;

   @Test
   public void testSensitiveReturningMethod() {
      setScopeResult(true);

      bean.sensitiveReturningMethod();

      setScopeResult(false);

      bean.sensitiveReturningMethod();
   }

   @Test
   public void testMethodWithSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithSensitiveParameter(new Sensitive());

      setScopeResult(false);

      bean.methodWithSensitiveParameter(new Sensitive());
   }

   @Test
   public void testAnnotatedSensitiveReturningMethod() {
      setScopeResult(true);

      bean.annotatedSensitiveReturningMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.annotatedSensitiveReturningMethod())
            .withMessage(
                  "Scope check failed for 'Sensitive[]' return object of 'com.tinubu.wallkeeper.aspect.trigger.NotAnnotatedBeanWithAnnotatedType::annotatedSensitiveReturningMethod' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithAnnotatedSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithAnnotatedSensitiveParameter(new Sensitive());

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithAnnotatedSensitiveParameter(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.NotAnnotatedBeanWithAnnotatedType::methodWithAnnotatedSensitiveParameter' operation using 'null' scope context");
   }

   @Test
   public void testAnnotatedNonSensitiveReturningMethod() {
      setScopeResult(true);

      bean.annotatedNonSensitiveReturningMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.annotatedNonSensitiveReturningMethod())
            .withMessage(
                  "Scope check failed for 'NonSensitive[]' return object of 'com.tinubu.wallkeeper.aspect.trigger.NotAnnotatedBeanWithAnnotatedType::annotatedNonSensitiveReturningMethod' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithAnnotatedNonSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithAnnotatedNonSensitiveParameter(new NonSensitive());

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithAnnotatedNonSensitiveParameter(new NonSensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=NonSensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.NotAnnotatedBeanWithAnnotatedType::methodWithAnnotatedNonSensitiveParameter' operation using 'null' scope context");
   }

   @Test
   public void testAnnotatedOptionalSensitiveReturningMethod() {
      setScopeResult(true);

      bean.annotatedOptionalSensitiveReturningMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.annotatedOptionalSensitiveReturningMethod())
            .withMessage(
                  "Scope check failed for 'Optional[Sensitive[]]' return object of 'com.tinubu.wallkeeper.aspect.trigger.NotAnnotatedBeanWithAnnotatedType::annotatedOptionalSensitiveReturningMethod' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithAnnotatedOptionalSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithAnnotatedOptionalSensitiveParameter(optional(new Sensitive()));

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithAnnotatedOptionalSensitiveParameter(optional(new Sensitive())))
            .withMessage(
                  "Scope check failed for 'parameter=Optional[Sensitive[]]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.NotAnnotatedBeanWithAnnotatedType::methodWithAnnotatedOptionalSensitiveParameter' operation using 'null' scope context");
   }

   @Test
   public void testAnnotatedIterableSensitiveReturningMethod() {
      setScopeResult(true);

      bean.annotatedIterableSensitiveReturningMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.annotatedIterableSensitiveReturningMethod())
            .withMessage(
                  "Scope check failed for '[Sensitive[]]' return object of 'com.tinubu.wallkeeper.aspect.trigger.NotAnnotatedBeanWithAnnotatedType::annotatedIterableSensitiveReturningMethod' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithAnnotatedIterableSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithAnnotatedIterableSensitiveParameter(Arrays.asList(new Sensitive()));

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithAnnotatedIterableSensitiveParameter(Arrays.asList(new Sensitive())))
            .withMessage(
                  "Scope check failed for 'parameter=[Sensitive[]]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.NotAnnotatedBeanWithAnnotatedType::methodWithAnnotatedIterableSensitiveParameter' operation using 'null' scope context");
   }

}