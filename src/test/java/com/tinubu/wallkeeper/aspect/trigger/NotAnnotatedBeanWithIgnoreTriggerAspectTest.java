/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.trigger;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.tinubu.wallkeeper.ParameterScopeException;
import com.tinubu.wallkeeper.aspect.AbstractScopeAspectTest;
import com.tinubu.wallkeeper.type.Registered;

@ContextConfiguration(classes = NotAnnotatedBeanWithIgnoreTriggerAspectTest.class)
public class NotAnnotatedBeanWithIgnoreTriggerAspectTest extends AbstractScopeAspectTest {

   @Autowired
   NotAnnotatedBeanWithIgnore bean;

   @Test
   public void testIgnoredMethod() {
      setScopeResult(true);

      bean.ignoredMethod();

      setScopeResult(false);

      bean.ignoredMethod();
   }

   @Test
   public void testIgnoredMethodWithNotAnnotatedParameter() {
      setScopeResult(true);

      bean.ignoredMethodWithNotAnnotatedParameter(new Registered());

      setScopeResult(false);

      bean.ignoredMethodWithNotAnnotatedParameter(new Registered());
   }

   @Test
   public void testIgnoredMethodWithAnnotatedParameter() {
      setScopeResult(true);

      bean.ignoredMethodWithAnnotatedParameter(new Registered());

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.ignoredMethodWithAnnotatedParameter(new Registered()))
            .withMessage(
                  "Scope check failed for 'parameter=Registered[value='any']' parameter of 'com.tinubu.wallkeeper.aspect.trigger.NotAnnotatedBeanWithIgnore::ignoredMethodWithAnnotatedParameter' operation using 'null' scope context");
   }

   @Test
   public void testIgnoredMethodWithIgnoredParameter() {
      setScopeResult(true);

      bean.ignoredMethodWithIgnoredParameter(new Registered());

      setScopeResult(false);

      bean.ignoredMethodWithIgnoredParameter(new Registered());
   }

   @Test
   public void testNotIgnoredMethodWithIgnoredParameter() {
      setScopeResult(true);

      bean.notIgnoredMethodWithIgnoredParameter(new Registered());

      setScopeResult(false);

      bean.notIgnoredMethodWithIgnoredParameter(new Registered());
   }

}