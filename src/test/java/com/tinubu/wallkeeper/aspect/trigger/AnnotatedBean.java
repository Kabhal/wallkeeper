/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.trigger;

import org.springframework.stereotype.Component;

import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.type.Registered;
import com.tinubu.wallkeeper.type.UnRegistered;

@Component
@ScopedBean
public class AnnotatedBean {

   public void voidReturningNotAnnotatedMethod() {}

   public UnRegistered unRegisteredReturningNotAnnotatedMethod() {
      return new UnRegistered();
   }

   public UnRegistered unRegisteredReturningNotAnnotatedMethodWithAnnotatedParameter(
         @Scoped Registered parameter) {
      return new UnRegistered();
   }

   public Registered notAnnotatedMethod() {
      return new Registered();
   }

   public Registered notAnnotatedMethodWithNotAnnotatedParameter(Registered parameter) {
      return new Registered();
   }

   public Registered notAnnotatedMethodWithAnnotatedParameter(@Scoped Registered parameter) {
      return new Registered();
   }

   @Scoped
   public void voidReturningAnnotatedMethod() {}

   @Scoped
   public UnRegistered unRegisteredReturningAnnotatedMethod() {
      return new UnRegistered();
   }

   @Scoped
   public Registered annotatedMethod() {
      return new Registered();
   }

   @Scoped
   public Registered annotatedMethodWithAnnotatedParameter(@Scoped Registered parameter) {
      return new Registered();
   }

   @Scoped
   public Registered annotatedMethodWithNotAnnotatedParameter(Registered parameter) {
      return new Registered();
   }

}
