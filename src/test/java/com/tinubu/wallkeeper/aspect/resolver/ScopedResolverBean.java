/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.resolver;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.ScopedIgnored;
import com.tinubu.wallkeeper.ScopedResolver;
import com.tinubu.wallkeeper.type.Sensitive;

@Component
@ScopedBean
public class ScopedResolverBean {

   public void scopedDefaultScopedResolver(@Scoped(resolver = @ScopedResolver()) Sensitive parameter) {
   }

   public @Scoped(resolver = @ScopedResolver())
   Sensitive scopedDefaultScopedResolver() {
      return new Sensitive();
   }

   public void scopedScopedResolver(@Scoped(
         resolver = @ScopedResolver("@scopeResolver.mirror(new com.tinubu.wallkeeper.type.Registered())"))
                                          Sensitive parameter) {
   }

   public @Scoped(
         resolver = @ScopedResolver("@scopeResolver.mirror(new com.tinubu.wallkeeper.type.Registered())"))
   Sensitive scopedScopedResolver() {
      return new Sensitive();
   }

   public void scopedInvalidScopedResolver(
         @Scoped(resolver = @ScopedResolver("##INVALID SpEL##")) Sensitive parameter) {
   }

   public @Scoped(resolver = @ScopedResolver("##INVALID SpEL##"))
   Sensitive scopedInvalidScopedResolver() {
      return new Sensitive();
   }

   public void scopedBeanReferencingScopedResolver(@Scoped(
         resolver = @ScopedResolver("@scopeResolver.mirror(new com.tinubu.wallkeeper.type.Registered())"))
                                                         Sensitive parameter) {
   }

   public @Scoped(
         resolver = @ScopedResolver("@scopeResolver.mirror(new com.tinubu.wallkeeper.type.Registered())"))
   Sensitive scopedBeanReferencingScopedResolver() {
      return new Sensitive();
   }

   public void scopedObjectReferencingScopedResolver(
         @Scoped(resolver = @ScopedResolver("@scopeResolver.mirror(scopedObject)")) Sensitive parameter) {
   }

   public @Scoped(resolver = @ScopedResolver("@scopeResolver.mirror(scopedObject)"))
   Sensitive scopedObjectReferencingScopedResolver() {
      return new Sensitive();
   }

   public void scopedParameterReferencingScopedResolver(
         @Scoped(resolver = @ScopedResolver("@scopeResolver.mirror(#hint)")) String parameter,
         @Scoped(ignore = @ScopedIgnored) Sensitive hint) {
   }

   public @Scoped(resolver = @ScopedResolver("@scopeResolver.mirror(#hint)"))
   String scopedParameterReferencingScopedResolver(@Scoped(ignore = @ScopedIgnored) Sensitive hint) {
      return new String();
   }

   @Scoped(resolver = @ScopedResolver("@scopeResolver.mirror(new com.tinubu.wallkeeper.type.Registered())"))
   public void scopedScopedResolverMethod(Sensitive parameter) {
   }

   @ScopedResolver("@scopeResolver.mirror(new com.tinubu.wallkeeper.type.Registered())")
   public void scopedResolverMethod(Sensitive parameter) {
   }

   @ScopedResolver("@scopeResolver.mirror(new com.tinubu.wallkeeper.type.UnRegistered())")
   public void scopedResolverMethodWithOverride(@Scoped(
         resolver = @ScopedResolver("@scopeResolver.mirror(new com.tinubu.wallkeeper.type.Registered())"))
                                                      Sensitive parameter) {
   }

   public void scopedResolverWhenOptionalParameter(@Scoped(
         resolver = @ScopedResolver("@scopeResolver.mirror(new com.tinubu.wallkeeper.type.Registered())"))
                                                         Optional<Sensitive> parameter) {
   }

   public void scopedResolverWhenIterableParameter(@Scoped(
         resolver = @ScopedResolver("@scopeResolver.mirror(new com.tinubu.wallkeeper.type.Registered())"))
                                                         List<Sensitive> parameter) {
   }

   public void scopedResolverWhenComposedParameter(@Scoped(
         resolver = @ScopedResolver("@scopeResolver.mirror(new com.tinubu.wallkeeper.type.Registered())"))
                                                         Optional<List<Sensitive>> parameter) {
   }

   public void scopedResolverWhenStreamParameter(@Scoped(
         resolver = @ScopedResolver("@scopeResolver.mirror(new com.tinubu.wallkeeper.type.Registered())"))
                                                       Stream<Sensitive> parameter) {
      parameter.collect(toList()); // effectively read the stream.
   }

   public @Scoped(
         resolver = @ScopedResolver("@scopeResolver.mirror(new com.tinubu.wallkeeper.type.Registered())"))
   Stream<Sensitive> scopedResolverWhenStreamReturnObject(
         @Scoped(ignore = @ScopedIgnored) Stream<Sensitive> parameter) {
      return parameter;
   }

}

