/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.specification;

import org.springframework.stereotype.Component;

import com.tinubu.commons.ddd2.domain.specification.CompositeSpecification;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.type.Registered;
import com.tinubu.wallkeeper.type.UnRegistered;

@Component
public class NotAnnotatedSpecificationBean {

   public boolean methodWithRegisteredSpecificationParameter(
         @Scoped Specification<Registered> specification) {
      return specification.satisfiedBy(new Registered());
   }

   public boolean methodWithUnRegisteredSpecificationParameter(
         @Scoped Specification<UnRegistered> specification) {
      return specification.satisfiedBy(new UnRegistered());
   }

   @Scoped
   public Specification<Registered> registeredSpecificationReturningMethod() {
      return registered -> true;
   }

   @Scoped
   public Specification<UnRegistered> unRegisteredSpecificationReturningMethod() {
      return unRegistered -> true;
   }

   public boolean methodWithRegisteredCompositeSpecificationParameter(
         @Scoped CompositeSpecification<Registered> specification) {
      return specification.satisfiedBy(new Registered());
   }

   public boolean methodWithUnRegisteredCompositeSpecificationParameter(
         @Scoped CompositeSpecification<UnRegistered> specification) {
      return specification.satisfiedBy(new UnRegistered());
   }

   @Scoped
   public CompositeSpecification<Registered> registeredCompositeSpecificationReturningMethod() {
      return registered -> true;
   }

   @Scoped
   public CompositeSpecification<UnRegistered> unRegisteredCompositeSpecificationReturningMethod() {
      return unRegistered -> true;
   }

}