/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.specification;

import org.springframework.stereotype.Component;

import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.ScopedIgnored;
import com.tinubu.wallkeeper.type.NonSensitive;
import com.tinubu.wallkeeper.type.Registered;
import com.tinubu.wallkeeper.type.Sensitive;
import com.tinubu.wallkeeper.type.UnRegistered;

@Component
@ScopedBean
public class AnnotatedSpecificationBean {

   @Scoped(ignore = @ScopedIgnored)
   public boolean methodWithRegisteredSpecificationParameter(Specification<Registered> specification) {
      return specification.satisfiedBy(new Registered());
   }

   @Scoped(ignore = @ScopedIgnored)
   public boolean methodWithUnRegisteredSpecificationParameter(Specification<UnRegistered> specification) {
      return specification.satisfiedBy(new UnRegistered());
   }

   @Scoped(ignore = @ScopedIgnored)
   public boolean methodWithSensitiveSpecificationParameter(Specification<Sensitive> specification) {
      return specification.satisfiedBy(new Sensitive());
   }

   @Scoped(ignore = @ScopedIgnored)
   public boolean methodWithNonSensitiveSpecificationParameter(Specification<NonSensitive> specification) {
      return specification.satisfiedBy(new NonSensitive());
   }

   @Scoped(ignore = @ScopedIgnored)
   public boolean methodWithIgnoredSensitiveSpecificationParameter(
         @Scoped(ignore = @ScopedIgnored) Specification<Sensitive> specification) {
      return specification.satisfiedBy(new Sensitive());
   }

   @Scoped(ignore = @ScopedIgnored)
   public boolean methodWithAnnotatedNonSensitiveSpecificationParameter(
         @Scoped Specification<NonSensitive> specification) {
      return specification.satisfiedBy(new NonSensitive());
   }

}