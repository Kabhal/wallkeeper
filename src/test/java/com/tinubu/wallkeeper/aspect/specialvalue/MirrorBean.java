/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.specialvalue;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.ScopedContext;
import com.tinubu.wallkeeper.type.Registered;

@Component
public class MirrorBean {

   @Scoped
   public Registered mirrorMethod(@Scoped Registered parameter) {
      return parameter;
   }

   @Scoped
   public Optional<Registered> mirrorMethod(@Scoped Optional<Registered> parameter) {
      return parameter;
   }

   @Scoped
   public List<Registered> mirrorMethod(@Scoped List<Registered> parameter) {
      return parameter;
   }

   @Scoped
   public Stream<Registered> mirrorMethod(@Scoped Stream<Registered> parameter) {
      return parameter;
   }

   @Scoped
   public Optional<?> untypedMirrorMethod(@Scoped Optional<?> parameter) {
      return parameter;
   }

   @Scoped
   public List<?> untypedMirrorMethod(@Scoped List<?> parameter) {
      return parameter;
   }

   @Scoped
   public Stream<?> untypedMirrorMethod(@Scoped Stream<?> parameter) {
      return parameter;
   }

   @Scoped
   @ScopedContext("#parameter")
   public Registered nullParameter(@Scoped Registered parameter) {
      return new Registered();
   }

}
