/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.scopedcontext;

import org.springframework.stereotype.Component;

import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.ScopedContext;
import com.tinubu.wallkeeper.type.Sensitive;

@Component
@ScopedBean
@ScopedContext("@scopeContext.read()")
public class AnnotatedScopedContextBean {

   public void scopedContextBeanMethod(Sensitive parameter) {}

   @ScopedContext("@scopeContext.write()")
   public void scopedContextBeanMethodWithMethodOverride(Sensitive parameter) {
   }

   public void scopedContextBeanMethodWithParameterOverride(
         @Scoped(ctx = @ScopedContext("@scopeContext.write()")) Sensitive parameter) {
   }

   @ScopedContext("@scopeContext.read()")
   public void scopedContextBeanMethodWithMethodAndParameterOverride(
         @Scoped(ctx = @ScopedContext("@scopeContext.write()")) Sensitive parameter) {
   }

}

