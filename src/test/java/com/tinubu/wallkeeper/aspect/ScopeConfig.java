/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.wallkeeper.aspect.TestScopeContext.Type.FAIL;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Objects;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.tinubu.wallkeeper.ScopeContext;
import com.tinubu.wallkeeper.ScopeFactory;
import com.tinubu.wallkeeper.UserScope;
import com.tinubu.wallkeeper.UserScopeProvider;
import com.tinubu.wallkeeper.aspect.AbstractScopeAspectTest.TestPlatformTransactionManager;
import com.tinubu.wallkeeper.type.AnnotatedSuperTypeSensitive;
import com.tinubu.wallkeeper.type.FailSensitive;
import com.tinubu.wallkeeper.type.MetaAnnotatedSensitive;
import com.tinubu.wallkeeper.type.NonSensitive;
import com.tinubu.wallkeeper.type.Registered;
import com.tinubu.wallkeeper.type.Sensitive;

@Configuration
public class ScopeConfig {

   public static ScopeFactory<ScopeContext, UserScope> scopeFactory;

   @Bean
   public TestPlatformTransactionManager platformTransactionManager() {
      return new TestPlatformTransactionManager();
   }

   /**
    * Provides a testing {@link UserScopeProvider}. In scope/out of scope switch flag is simulated with {@link
    * TestingAuthenticationToken#getDetails()} which contains a boolean.
    *
    * @return testing scope provider
    */
   @Bean
   public UserScopeProvider<UserScope> userScopeProvider() {
      return () -> {
         Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

         assertThat(authentication).isInstanceOf(TestingAuthenticationToken.class);

         Boolean scoped = (Boolean) authentication.getDetails();

         if (scoped) {
            return optional(new TestUserScope());
         } else {
            return optional();
         }
      };
   }

   /**
    * Creates a controllable scope specification for test purpose. Using current security context
    * authentication scoped value to control test cases.
    */
   @Bean
   public ScopeFactory<ScopeContext, UserScope> scopeFactory() {
      scopeFactory = new ScopeFactory<>();

      scopeFactory.registerScope(Registered.class,
                                 (context, userScope) -> __ -> userScope.isPresent()
                                                               && !failContext(context));
      scopeFactory.registerScope(Sensitive.class,
                                 (context, userScope) -> __ -> userScope.isPresent()
                                                               && !failContext(context));
      scopeFactory.registerScope(FailSensitive.class,
                                 (context, userScope) -> __ -> userScope.isPresent()
                                                               && !failContext(context));
      scopeFactory.registerScope(AnnotatedSuperTypeSensitive.class,
                                 (context, userScope) -> __ -> userScope.isPresent()
                                                               && !failContext(context));
      scopeFactory.registerScope(MetaAnnotatedSensitive.class,
                                 (context, userScope) -> __ -> userScope.isPresent()
                                                               && !failContext(context));
      scopeFactory.registerScope(NonSensitive.class,
                                 (context, userScope) -> __ -> userScope.isPresent()
                                                               && !failContext(context));

      return scopeFactory;
   }

   private static boolean failContext(ScopeContext context) {
      return context != null && context.sameContextAs(new TestScopeContext(FAIL));
   }

   public static class TestUserScope implements UserScope {
      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         return true;
      }

      @Override
      public int hashCode() {
         return Objects.hash();
      }
   }
}
