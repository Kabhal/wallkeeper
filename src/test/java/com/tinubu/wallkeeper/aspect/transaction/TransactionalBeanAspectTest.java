/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.transaction;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;

import com.tinubu.wallkeeper.aspect.AbstractScopeAspectTest;
import com.tinubu.wallkeeper.type.Registered;

@ContextConfiguration(classes = TransactionalBeanAspectTest.class)
public class TransactionalBeanAspectTest extends AbstractScopeAspectTest {

   @Autowired
   TransactionalBean bean;

   @Autowired
   TestPlatformTransactionManager transactionManager;

   @Test
   @Transactional
   public void testTransactionalMethod() {
      setScopeResult(true);

      assertThat(TestTransaction.isActive()).isTrue();

      bean.transactionalMethod(new Registered());

      assertThat(transactionManager.createCount()).isEqualTo(1);
   }

}