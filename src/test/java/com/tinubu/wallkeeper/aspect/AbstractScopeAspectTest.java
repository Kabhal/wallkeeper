/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect;

import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.SimpleTransactionStatus;

import com.tinubu.wallkeeper.autoconfigure.ScopeAutoConfiguration;

@SpringBootTest
@ActiveProfiles("test")
@ImportAutoConfiguration({ AopAutoConfiguration.class, ScopeAutoConfiguration.class })
@ComponentScan(basePackageClasses = AbstractScopeAspectTest.class)
public abstract class AbstractScopeAspectTest {

   protected void setAuthentication(Authentication authentication) {
      SecurityContextHolder.getContext().setAuthentication(authentication);
   }

   /**
    * Switches "scope" rule evaluation result to easily test methods when scope rule evaluation successes or
    * fails.
    * The state will change the test user "scoped" state which is used in scope rule evaluation to simulate
    * a successful or failed evaluation.
    */
   protected void setScopeResult(boolean scopeResult) {
      TestingAuthenticationToken authentication = new TestingAuthenticationToken("user", null);
      authentication.setDetails(scopeResult);

      setAuthentication(authentication);
   }

   private URL url(String url) {
      try {
         return new URL(url);
      } catch (MalformedURLException e) {
         throw new IllegalStateException(e);
      }
   }

   /** Instrumented {@link PlatformTransactionManager} implementation for test. */
   public static class TestPlatformTransactionManager implements PlatformTransactionManager {

      private int createCount = 0;

      @Override
      public TransactionStatus getTransaction(TransactionDefinition definition) throws TransactionException {
         createCount++;
         return new SimpleTransactionStatus();
      }

      @Override
      public void commit(TransactionStatus status) throws TransactionException {}

      @Override
      public void rollback(TransactionStatus status) throws TransactionException {}

      public int createCount() {
         return createCount;
      }
   }
}