= Wallkeeper
:gitlab-url: https://gitlab.com
:default-branch: master
:project-path: tinubu-public/wallkeeper
:toc:

image:{gitlab-url}/{project-path}/badges/{default-branch}/pipeline.svg[link="{gitlab-url}/{project-path}/commits/{default-branch}",title="pipeline status"]
image:{gitlab-url}/{project-path}/badges/{default-branch}/coverage.svg[link="{gitlab-url}/{project-path}/commits/{default-branch}",title="coverage report"]

== Design

Wallkeeper framework aimed to implement security authorization _resource scopes_ into applications. _Resource scopes_ is a mean to restrict user access to a subset of resources in a resource pool, depending on authenticated _user scope_ and rules for the resource, specific to each application.
Library design is generic so that a specific application can define its own _user scope_ and _resource scopes_.

Walkeeper works with Spring/Spring Boot applications, and uses Spring AOP to auto-detect method to check for scope.

== Principles

* Reusable, all-in-one, scope specifications : _Resource scopes_ are defined as https://www.martinfowler.com/apsupp/spec.pdf[*Specifications*].
These specifications can be used to check if a resource is in a scope or be used to request all resources in current _user scope_, by correctly implementing https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#specifications[Spring Data JPA specifications]
* Centralized definitions : _Resource scopes_ are defined *once* and in a *central location* (in a _scope factory registry_)
* Secured by default : Methods to check can be annotated `@Scoped` individually, but preferred usage is to mark a class `@ScopedBean` so that all methods are secured by default
* Securing both input parameters and returned resources : Methods parameters can be individually annotated to be checked
* No silently bypassed scopes : Any explicitly annotated method, or parameter, or any method of a `@ScopedBean` annotated class must have a registered scope, or application will fail immediately
* Powerful support for scoped resources in any combination of `Iterable` (including `Page`), `Optional`, `Stream` containers

The framework will basically issue 3 exceptions :

* `ParameterScopeException` (implements AccessDeniedException) : A resource passed as a method input parameter is not in its defined scope, you should map this error to a _user error_, for example, returning a 40x error
* `NotRegisteredScopeException` (implements RuntimeException) : This is a developer error, a _scoped resource_ does not have any registered scope associated in _scope factory_
* `ReturnObjectScopeException` (implements RuntimeException) : This is a developer error, a returned _scoped resource_ is not in its defined scope.
The developer should have correctly implemented the method so that this case never occur

From a scope checking perspective, the absence of data is not considered an error, so the following values are always considered in scope :

* `null` values, `null` containers (Optional, Iterable, ...)
* empty `Iterable`
* empty `Optional`

== Basic Usage

[source,java]
.Define scope registry
----
import com.tinubu.wallkeeper.UserScope;
import com.tinubu.wallkeeper.ScopeContext;
import com.tinubu.wallkeeper.ScopeFactory;
import com.tinubu.wallkeeper.UserScopeProvider;
import com.tinubu.wallkeeper.ScopeSpecificationFactory;

@Configuration
public class ScopeConfig {

   @Bean
   public ScopeFactory<ScopeContext, MyUserScope> scopeFactory() { // <1>
      ScopeFactory<ScopeContext, MyUserScope> scopeFactory = new ScopeFactory<>();

      scopeFactory.registerScope(MyResource.class, myResourceScopeFactory()); // <2>
      scopeFactory.registerDefaultScope(MyPublicResource.class); // <3>

      return scopeFactory;
   }

   @Bean
   public UserScopeProvider<MyUserScope> userScopeProvider() { // <4>
      return () -> Optional.of(new MyUserScope());
   }

   public static class MyUserScope implements UserScope {
      int scopeId() { ... }
   }

   private ScopeSpecificationFactory<MyResource, ScopeContext, MyUserScope> myResourceScopeFactory() {
      return new CachingScopeSpecificationFactory<>() { // <5>
         @Override
         protected Specification<MyResource> buildScopeSpecification(ScopeContext context,
                                                                     Optional<MyUserScope> scope) {
            return myResource -> scope.map(userScope -> myResource.scopeId().equals(userScope.scopeId())).orElse(true); // <6>
         }
      };
   }
}
----

<1> Define the _scope factory_ with `ScopeContext` and `UserScope` types for the whole application
<2> Register `MyResource` into the factory
<3> Register `MyPublicResource` into the factory with a default rule that consider all resources in scope.
With this default registering, you won't have to explicitly ignore the scope check in methods, when scope is required, for this resource
<4> Provide your definition of a _user scope_, generally based on current user `Authentication` from Spring's `SecurityContext`
<5> Define `MyResource` scope specification factory using a `CachingScopeSpecificationFactory`
<6> Implement scope rule for `MyResource`, the following is just a sample

[source,java]
.Explicitly and individually enable scope checking for returned resource
----
import com.tinubu.wallkeeper.Scoped;

public class MyService {
  @Scoped MyResource readResource(MyResourceId myResourceId) { ... }
}
----

[source,java]
.Check resource scope in an optional list container without any extra configuration
----
import com.tinubu.wallkeeper.Scoped;

public class MyService {
  @Scoped Optional<List<MyResource>> readResource(MyResourceId myResourceId) { ... }
}
----

[source,java]
.Globally enable scope checking on all *public* methods
----
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.Scoped;

@ScopedBean
public class MyService {
  MyResource readResource(MyResourceId myResourceId) { ... }
}
----

[source,java]
.Ignore a method in a scoped bean
----
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.ScopedIgnored;

@ScopedBean
public class MyService {
  MyResource readResource(MyResourceId myResourceId) { ... }

  @Scoped(ignore = @ScopedIgnored) MyPublicResource readPublicResource(MyResourceId myResourceId) { ... }
}
----

[[advanced-usage]]
== Advanced usage

=== Check parameter scopes

Method input parameters can be also checked, but need to be explicitly annotated with `@Scoped`.
Method returned resource and parameters are managed individually, *`@ScopedBean` does not activate scope checking for all methods parameters*.

[source,java]
.Enable scope check on MyResource parameter
----
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.Scoped;

@ScopedBean
public class MyService {
  MyResource activateResource(@Scoped MyResource myResource) { ... }
}
----

If you specify your resource with an identifier instead of passing the resource object, you can use a resolver to map the identifier to the resource object.

[source,java]
.Enable scope check on MyResource parameter
----
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.ScopedResolver;

@ScopedBean
@Transactional
public class MyService {
  MyResource activateResource(@Scoped(resolver = @ScopedResolver("@scopeResolver.resolve(scopedResource)")) MyResourceId myResourceId) { ... }
}
----

The `@ScopedResolver` annotation takes a <<SpEL expression>> to resolve the `MyResourceId` parameter to a `MyResource` object.
Here we used a specific bean named `scopeResolver` to do the work.

This bean can be declared in Spring context, for example :

[source,java]
----
@Configuration
public class ScopeConfig {

   @Component("scopeResolver")
   public static class ScopeResolver {

      @Autowired
      MyResourceRepository myResourceRepository;

      public Optional<MyResource> resolve(MyResourceId myResourceId) {
         return Optional.ofNullable(myResourceId).flatMap(myResourceRepository::findMyResourceById);
      }

      /**
       * Fallback identity resolver. Useful when this scope resolver is defined globally for all scoped resource.
       */
      public Object resolve(Object object) {
         return object;
      }
   }
}
----

[NOTE]
====
When `@ScopedResolver` is applied to `Optional`, `Iterable` or `Stream` containers, the expression applies to each container item, not the container itself.

[source,java]
.Resolves container items
----
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.ScopedResolver;

@ScopedBean
@Transactional
public class MyService {
MyResource activateResource(@Scoped(resolver = @ScopedResolver("@scopeResolver.resolve(scopedResource)")) List<MyResourceId> myResourceIds) { ... }
}
----

In this case, `scopeResolver.resolve` resolves each `MyResourceId` of the list independently.
====

[TIP]
====
If a transaction is required by the scope framework, by example in the resolver above, you can annotate your scoped method with `@Transactional` :

* the same transaction will be used in the scope framework and in the regular method body
* you can benefit from persistence caching if resource is searched again in the method body (using https://vladmihalcea.com/jpa-hibernate-first-level-cache/[Spring JPA L1 cache] for example)
====

=== Using advanced scope context

Sometime, the information contained in a resource is not sufficient to implement the scope rule for it.
Or the scope rule can differ for the same resource depending on the operation, ...
In these cases, a _scope context_ can be added to scope as an <<SpEL expression>>.
This scope context is passed to _scope factory_ and can be used to implement the scope rule for a given resource.

CAUTION: Scope context must correctly implements `equals`/`hashCode` so that scope factory caching works correctly.

==== Simple scope context

[source,java]
----
@Configuration
public class ScopeConfig {

   public enum ScopeContext implements com.tinubu.wallkeeper.ScopeContext {
      READ, WRITE
   }
}
----

[source,java]
.Simple scope context for MyResourceId parameter scope check
----
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.ScopedContext;

@ScopedBean
public class MyService {
  MyResource readResource(MyResourceId myResourceId) { ... }

  MyResource readResource(@Scoped(ctx = @ScopedContext("T(my.ScopeContext).READ")) MyResourceId myResourceId) { ... }
}
----

==== Advanced scope context

[source,java]
----
@Configuration
public class ScopeConfig {

   public enum ScopeContext implements com.tinubu.wallkeeper.ScopeContext {
      READ, WRITE
   }

   @Component("scopeContext")
   public static class ScopeContextFactory {

      private HintId hintId;

      public ScopeContext read(HintId hintId) {
         this.hintId = hintId;
         return READ;
      }

      public ScopeContext write(HintId hintId) {
         this.hintId = hintId;
         return WRITE;
      }

}
----

[source,java]
.Advanced scope context for MyResourceId parameter, referencing another parameter
----
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.ScopedContext;

@ScopedBean
public class MyService {
  MyResource readResource(MyResourceId myResourceId) { ... }

  MyResource readResource(@Scoped(ctx = @ScopedContext("@scopeContext.read(#hintId)")) MyResourceId myResourceId, HintId hintId) { ... }
}
----

=== Scope context and resolver factorization

For advanced configurations, you can use directly `@ScopedContext` and `@ScopedResolver` to factorize configuration :

* on a method, to factorize configuration for returning resources and parameters
* on a class, to factorize configuration for all methods returning resources and parameters

You can then override locally the scope context or resolver.

[source,java]
----
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.ScopedContext;
import com.tinubu.wallkeeper.ScopedResolver;

@ScopedBean
@ScopedContext("@scopeContext.read()")
@ScopedResolver("@scopeResolver.resolve(scopedObject)")
public class MyService {
MyResource readResource(MyResourceId myResourceId) { ... }

  MyResource readPublicResource(@Scoped MyResourceId myResourceId) { ... }

  MyResource readHintedResource(@Scoped(ctx = @ScopedContext("@scopeContext.read(#hintId)")) MyResourceId myResourceId, @Scoped HintId hintId) { ... }
}
----

Note that in the example above, `hintId` is also checked for scope as it's a user input.

=== Systematic resource scope checking

Returning resources or all *public* methods of classes annotated with `@ScopedBean` are checked for scope.
But parameters must be explicitly annotated with `@Scoped`.
You can be more systematic by directly annotate, or https://github.com/spring-projects/spring-framework/wiki/Spring-Annotation-Programming-Model[meta-annotate], or annotate super-interface of, any resource, with `@Scoped`, so that any such resource is automatically checked for scope, *but only in `@ScopedBean` annotated beans* that are <<condition,*not disabled by condition*>>.

[source,java]
.Systematically check for MyResource scope in both returning type and method input parameter
----
@Scoped
public class MyResource {}

@ScopedBean
public class MyService {

  MyResource activateResource(MyResource myResource) { ... }

}
----

NOTE: If you use an identifier as method parameter, e.g. `MyResourceId`, identifier must be annotated too.
Evidently this can't be done when working with primitive types.

NOTE: This also work if annotated resource, is contained in any combination of `Optional`, `Iterable`, `Stream` containers

[[advanced-usage-multiple-scopes]]
=== Multiple scope rules on a single return object or parameter

`@Scoped` annotation is `Repeatable`, so that you can define multiple scope rules using different resolvers/contexts on a single resource, or use the `@ScopedSet` annotation with the same result.
This work at all places where a single `@Scoped` annotation is supported.

IMPORTANT: In this cases, all defined rules will have to be successful.

.Example use case for repeatable scope rules
====
[source,java]
----
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.Scoped;

@ScopedBean
public class MyService {
  MyResource activateResource(@Scoped(resolver = @ScopedResolver("scopedObject.value1()"))
                              @Scoped(resolver = @ScopedResolver("scopedObject.value2()"), ctx = @ScopedContext("@scopeContext.read(#hintId)"))
                              MyResource myResource, String hintId) { ... }
}
----
====

=== Special handling

==== Specification parameter handling

When a `com.tinubu.commons.ddd2.Specification` parameter is annotated with `@Scoped`, the framework behavior is to update the specification to add the _resource scope_ to it with an *and* operation, and pass it to the method body.
Consequently, the resulting `Specfication` can be converted to a `CompositeSpecification` for the needs.

NOTE: if specification value is `null`, the specification will be replaced with _resource scope_ specification.

CAUTION: the received specification correctly implements the scope while you don't apply *or* or *not* operations to it.
In this case, scoped specification correctness is *defeated*.
You can use `ScopeService::scopedSpecification` in method body to re-apply resource scope to updated specification (note that in this case, the security scope specification will be present at several places in the composite specification).

If you defined <<advanced-usage-multiple-scopes,multiple scopes on the specification>>, the specification will be updated with each of them.

== SpEL expression

Both _scope context_ and _scope resolver_ features use https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#expressions[SpEL expressions].

In addition to default expression mechanisms, these expressions support specifically :

* `scopedObject` : value referencing the currently annotated resource value
* `returnObject` : boolean value indicating that scoped object is a method return value if set to `true`, or a parameter value if set to `false`
* `#name` : any method parameter value, referenced by parameter name (note that `-parameters` must be enabled in compiler or parameters will be named `arg0`, `arg1`, ...)

These functions are available by default in all SpEL expressions :

* `hasProperty(String named, String matches, boolean matchIfMissing = false)` : JVM System property matches specified Regex
* `hasEnvironment(String named, String matches, boolean matchIfMissing = false)` : Environment variable matches specified Regex
* `hasSpringEnvironment(String named, String matches, boolean matchIfMissing = false)` : Spring environment property matches specified Regex
* `hasSpringProfile(String named)` : Spring profile is active

[TIP]
====
These functions are particularly useful in `@ScopedIgnored` expressions

[source,java]
.Disables scope checking only on return objects when not in tests
----
@ScopedIgnored("returnObject && !hasSpringProfile('test')")
----
====

== Build

[[build-git-flow]]
=== Git flow

https://nvie.com/posts/a-successful-git-branching-model/[Git-flow] is a standardized branch model.

We use the following Git flow configuration :

Development branches :

* Development branch : `master` (non default)
* Releases branch : `releases` (non default)

Branches prefixes :

* Feature : `feature/`
* release : `release/`
* hotfix : `hotfix/`
* support : `support/`

Release version tags : `release-` (non default)

[[build-versioning]]
=== Versioning

We use the https://semver.org[Semantic versioning] : `<major>.<minor>.<patch>`, e.g.: `1.0.2`.

When we choose the next version for the project after a release, we always start by incrementing the _<minor>_ number.
The _<patch>_ number is reserved for security/hot-fixes. +
If the development introduces some backwards incompatible changes, we must release the current version, then increment the _<major>_ number and reset the _<minor>_ number to 0 for next version.

[[build-release]]
=== Release procedure

<<build-git-flow,Git flow>> must be configured correctly.

The library release supervisor must follow the procedure :

1. Use `git-flow` to start the release : `git flow release start <release-version>`
2. Bump the current project version and external dependencies that are still in `-SNAPSHOT` on release branch :
* Set release version : `mvn versions:set -DnewVersion=<release-version>`
* External dependencies version must be manually updated
* You *must* run `mvn clean test` successfully
3. Use `git-flow` to finish the release : `git flow release finish <release-version>`
+
CAUTION: Do not push your changes now, before having bumped the development branch version, or library artifacts will be deployed to repositories with release version and will corrupt Maven repository caches on various locations
+
4. Bump the current project version on development branch following the <<build-versioning>> instructions.
+
NOTE: Only the current project version is required to be bumped to next snapshot version, external dependencies should only be bumped when required, and preferably later, in a different commit.
5. Push all your changes, *including the tags*

==== Scripted procedure

A script is available for both release and hotfix procedures, that enforces the release guidelines :

.Release script
[source,bash]
----
release.sh release <release-version> [<resume step number>]
----

.Hotfix script
[source,bash]
----
release.sh hotfix <hotfix-version> [<resume step number>]
----

